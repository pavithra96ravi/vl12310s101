/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/*Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

void SystemClock_Config(void);
void LoadStatusDefault();

int8_t AT24C32D_write(uint16_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len);
int8_t  AT24C32D_read(uint16_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len);

void PowerOn();
void PowerOff();

void SystemInitialze();

#define ENABLE  1
#define DISABLE 0


//struct var_LoadStatus{
//
//
//
//}LoadStatus;

/*
Relay-1 : BLW
Relay-2 : LHT
Relay-3 : UV
Relay-4 : WA
Relay-5 : MIC
Relay-6 : SYS
Relay-7 : CAM
Relay-8 : NIL
*/

//#define LIMIT 7 // 0 to 4
#define LIMIT 9
#define DEBOUNCE_DELAY HAL_Delay(250);

unsigned char LoadStatus[10];

bool LoadEn;
                          //   PW  BLW_CON  BLW_SPD   LHT     UV      WA       MIC     SYS     CAM
unsigned char LoadEnCon[10]={ENABLE,ENABLE, ENABLE , ENABLE, ENABLE, ENABLE, ENABLE,ENABLE,ENABLE};



GPIO_TypeDef* RelayConPort[8]={GPIOB,GPIOB,GPIOB, GPIOB , GPIOB  , GPIOB , GPIOB , GPIOB };
uint32_t RelayConPin[8]={GPIO_PIN_0,GPIO_PIN_1,GPIO_PIN_4,GPIO_PIN_5,GPIO_PIN_6,GPIO_PIN_7,GPIO_PIN_12,GPIO_PIN_13};


GPIO_TypeDef* LEDConPort[8]={GPIOA,GPIOA,GPIOA, GPIOA , GPIOA  , GPIOA , GPIOA , GPIOA };
uint32_t LEDConPin[8]={GPIO_PIN_3,GPIO_PIN_4,GPIO_PIN_5,GPIO_PIN_6,GPIO_PIN_7,GPIO_PIN_8,GPIO_PIN_11,GPIO_PIN_12};


int main(void)
{

  HAL_Init();

  SystemClock_Config();

  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();

  SystemInitialze();

/*
  unsigned char zero =0;
  for(int i=0;i<=10;i++){
  AT24C32D_write(i,&zero,1);
  HAL_Delay(100);
  }while(1);
*/
//for(int  i=0;i<30;i++)
//{
//	AT24C32D_write(2,0,1);
//	AT24C32D_write(1,1,1);

//	HAL_Delay(15);
//}
  AT24C32D_read(0x00,LoadStatus,9);

  LoadStatus[PW]=0x00;
  LoadStatus[BLW_CON]=0x00;
//  HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port, BLW_LEDA_Pin, SET);
//  HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port, BLW_LEDB_Pin, RESET);
//  LoadStatus[BLW_CON]=((~LoadStatus[BLW_CON])&0x01);
//	AT24C32D_write(0x01,&LoadStatus[BLW_CON],1);
// 	    LoadStatus[BLW_SPD]=((~LoadStatus[BLW_SPD])&0x01);
//
// 	    AT24C32D_write(BLW_SPD,&LoadStatus[BLW_SPD],1);


  while (1)
  {

	  	 if(!PW_SW){
	  		HAL_GPIO_TogglePin(PW_LEDA_GPIO_Port,PW_LEDA_Pin);
	  		HAL_GPIO_TogglePin(PW_LEDB_GPIO_Port,PW_LEDB_Pin);

	  		LoadStatus[PW]=((~LoadStatus[PW])&0x01);
	  		LoadStatus[BLW_CON]=((~LoadStatus[BLW_CON])&0x01);

	  		AT24C32D_write(0x00,&LoadStatus[PW],1);
	  		AT24C32D_write(0x01,&LoadStatus[BLW_CON],1);

	  		while(!PW_SW);

	  		    if(LoadStatus[PW]==0x01){ // Power ON
	  		    	PowerOn();
	  		    }
	  		    else if(LoadStatus[PW]==0x00){ // Power OFF
	  		    	PowerOff();
	  		    }

	  		  DEBOUNCE_DELAY
	  	    }
	  	 else if((!BLW_SW)&&(LoadStatus[PW])&&(LoadEnCon[BLW_SPD])){
	  		HAL_GPIO_TogglePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin);
	  		HAL_GPIO_TogglePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin);
	  		HAL_GPIO_TogglePin(BLW_SPD_GPIO_Port,BLW_SPD_Pin);//Load

   	  	    LoadStatus[BLW_SPD]=((~LoadStatus[BLW_SPD])&0x01);
   	  	    AT24C32D_write(BLW_SPD,&LoadStatus[BLW_SPD],1);
   	  	    while(!BLW_SW);

   	  	    DEBOUNCE_DELAY
 	  	    }
	  	 else if((!LHT_SW)&&(LoadStatus[PW])&&(LoadEnCon[LHT])){
	  		 HAL_GPIO_TogglePin(LHT_LED_GPIO_Port,LHT_LED_Pin);
	  		 HAL_GPIO_TogglePin(LHT_GPIO_Port,LHT_Pin); //Load

	  		 LoadStatus[LHT]=((~LoadStatus[LHT])&0x01);
	  		 AT24C32D_write(LHT,&LoadStatus[LHT],1);
	  		 while(!LHT_SW);
	  		 DEBOUNCE_DELAY
	  	     }
	       else  if((!UV_SW)&&(LoadStatus[PW])&&(LoadEnCon[UV])){
	  		 HAL_GPIO_TogglePin(UV_LED_GPIO_Port,UV_LED_Pin);
	  		 HAL_GPIO_TogglePin(UV_GPIO_Port,UV_Pin); //Load

	  		 LoadStatus[UV]=((~LoadStatus[UV])&0x01);
	  		 AT24C32D_write(UV,&LoadStatus[UV],1);
	  		 while(!UV_SW);
	  		DEBOUNCE_DELAY
	  	     }
	  	 else if((!WA_SW)&&(LoadStatus[PW])&&(LoadEnCon[WA])){
	  		 HAL_GPIO_TogglePin(WA_LED_GPIO_Port,WA_LED_Pin);
	  		 HAL_GPIO_TogglePin(WA_GPIO_Port,WA_Pin); //Load

	  		 LoadStatus[WA]=((~LoadStatus[WA])&0x01);
	  		 AT24C32D_write(WA,&LoadStatus[WA],1);
	  		 while(!WA_SW);
	  		DEBOUNCE_DELAY
	  	 	 }
	  	 else if((!MIC_SW)&&(LoadStatus[PW])&&(LoadEnCon[MIC])){
	  		 HAL_GPIO_TogglePin(MIC_LED_GPIO_Port,MIC_LED_Pin);
	  		 HAL_GPIO_TogglePin(MIC_GPIO_Port,MIC_Pin); //Load

	  		 LoadStatus[MIC]=((~LoadStatus[MIC])&0x01);
	  		 AT24C32D_write(MIC,&LoadStatus[MIC],1);
	  		 while(!MIC_SW);
	  		DEBOUNCE_DELAY
	  	     }
	  	 else if((!SYS_SW)&&(LoadStatus[PW])&&(LoadEnCon[SYS])){
	  		 HAL_GPIO_TogglePin(SYS_LED_GPIO_Port,SYS_LED_Pin);
	  		 HAL_GPIO_TogglePin(SYS_GPIO_Port,SYS_Pin); //Load

	  		 LoadStatus[SYS]=((~LoadStatus[SYS])&0x01);
	  		 AT24C32D_write(SYS,&LoadStatus[SYS],1);
	  		 while(!SYS_SW);
	  		DEBOUNCE_DELAY
	  	 	 }
//	  	 else if((!CAM_SW)&&(LoadStatus[PW])&&(LoadEnCon[CAM])){
//	  		 HAL_GPIO_TogglePin(CAM_LED_GPIO_Port,CAM_LED_Pin) ;
//	  		 HAL_GPIO_TogglePin(CAM_GPIO_Port,CAM_Pin); //Load
//
//	  		 LoadStatus[CAM]=((~LoadStatus[CAM])&0x01);
//	  		 AT24C32D_write(CAM,&LoadStatus[CAM],1);
//	  		 while(!CAM_SW);
//	  		DEBOUNCE_DELAY
//	  	 	 }


  }

}






int8_t  AT24C32D_read(uint16_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len)
{

	   // ...
	    // Please insert system specific function to read from bus where BME680 is connected
	    // ...

		//I2C1_Start();
		//I2C1_Write( 0x77, &reg_addr, 1, END_MODE_RESTART );
		//I2C1_Read( 0x77, reg_data_ptr, data_len, END_MODE_STOP );

	uint8_t tmp[10];
	tmp[0] = (reg_addr>>8)&(0x00FF);
	tmp[1] = (reg_addr)&(0x00FF);

	//memcpy( &tmp[2], reg_data_ptr, data_len );



		HAL_I2C_Master_Transmit(&hi2c1,AT24C32D_Add<<1,tmp,2,100);
		HAL_Delay(5);
		HAL_I2C_Master_Receive(&hi2c1,AT24C32D_Add<<1,reg_data_ptr,data_len,100);
		HAL_Delay(100);
	//	HAL_UART_Transmit(&huart1,"RD",2,5);
	//	ASCII_Conv(dev_addr);
	//	ASCII_Conv(data_len);
		//HAL_UART_Transmit(&huart1,temp,1,100);

		//HAL_Delay(2000);

	    return 0;
}


int8_t AT24C32D_write(uint16_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len)
{

	  // ...
	    // Please insert system specific function to write to the bus where BME680 is connected
	    // ...

		uint8_t tmp[256];
		tmp[0] = (reg_addr>>8)&(0x00FF);
		tmp[1] = (reg_addr)&(0x00FF);

		memcpy( &tmp[2], reg_data_ptr, data_len );
		//HAL_UART_Transmit(&huart1,&dev_addr,1,100);

		HAL_I2C_Master_Transmit(&hi2c1,AT24C32D_Add<<1,tmp,data_len+2,100);

	//	HAL_UART_Transmit(&huart1,"WR",2,5);
	    return 0;
}


void SystemInitialze(){



//LEDB-RED ; LEDA-GREEN;


	HAL_GPIO_WritePin(PW_LEDA_GPIO_Port,PW_LEDA_Pin,RESET); //Green
	HAL_GPIO_WritePin(PW_LEDA_GPIO_Port,PW_LEDB_Pin,SET);   //RED


	HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin,RESET); // Orange
	HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin,RESET);  //Red


	HAL_GPIO_WritePin(LHT_LED_GPIO_Port,LHT_LED_Pin,RESET);
	HAL_GPIO_WritePin(UV_LED_GPIO_Port,UV_LED_Pin,RESET);


	HAL_GPIO_WritePin(WA_LED_GPIO_Port,WA_LED_Pin,RESET);
	HAL_GPIO_WritePin(MIC_LED_GPIO_Port,MIC_LED_Pin,RESET);
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port,SYS_LED_Pin,RESET);
	HAL_GPIO_WritePin(CAM_LED_GPIO_Port,CAM_LED_Pin,RESET);




}

void PowerOff(){

    /***************** LOAD *************************/
	HAL_GPIO_WritePin(BLW_CON_GPIO_Port,BLW_CON_Pin,RESET);
	HAL_GPIO_WritePin(BLW_SPD_GPIO_Port,BLW_SPD_Pin,RESET);
	HAL_GPIO_WritePin(LHT_GPIO_Port,LHT_Pin,RESET);
	HAL_GPIO_WritePin(UV_GPIO_Port,UV_Pin,RESET);
	HAL_GPIO_WritePin(WA_GPIO_Port,WA_Pin,RESET);
	HAL_GPIO_WritePin(MIC_GPIO_Port,MIC_Pin,RESET);
	HAL_GPIO_WritePin(SYS_GPIO_Port,SYS_Pin,RESET);
	HAL_GPIO_WritePin(CAM_GPIO_Port,CAM_Pin,RESET);
   /**************************************************/


	/********************** LEDs **************************/
	HAL_GPIO_WritePin(PW_LEDA_GPIO_Port,PW_LEDA_Pin,RESET); //Green
	HAL_GPIO_WritePin(PW_LEDA_GPIO_Port,PW_LEDB_Pin,SET);   //RED


	HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin,RESET); // Orange
	HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin,RESET);  //Red


	HAL_GPIO_WritePin(LHT_LED_GPIO_Port,LHT_LED_Pin,RESET);
	HAL_GPIO_WritePin(UV_LED_GPIO_Port,UV_LED_Pin,RESET);

	HAL_GPIO_WritePin(WA_LED_GPIO_Port,WA_LED_Pin,RESET);
	HAL_GPIO_WritePin(MIC_LED_GPIO_Port,MIC_LED_Pin,RESET);
	HAL_GPIO_WritePin(SYS_LED_GPIO_Port,SYS_LED_Pin,RESET);
	HAL_GPIO_WritePin(CAM_LED_GPIO_Port,CAM_LED_Pin,RESET);
	/********************************************************/
}


void PowerOn(){
	AT24C32D_read(0x00,LoadStatus,9);

	for(unsigned char i=0;i<=LIMIT;i++){

//			if(LoadStatus[i]==0x01){
//				HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i]);
//			}
//			else if(LoadStatus[i]==0x01){
//				HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i]);
//			}


		       HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i+1]);


		        if((LoadStatus[BLW_CON]==1)&&(i==1)){
		        	HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin,RESET); // Orange
		        	HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin,RESET);  //Green
		        }
		        else if( (LoadStatus[BLW_CON]==1)&&(LoadStatus[BLW_SPD]==1)&&(i==2) ){
		        	HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin,SET); // Orange
		        	HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin,RESET);  //Green
		        }
		        else if( (LoadStatus[BLW_CON]==1)&&(LoadStatus[BLW_SPD]==0)&&(i==2) ){
		        	HAL_GPIO_WritePin(BLW_LEDA_GPIO_Port,BLW_LEDA_Pin,RESET); // Orange
		        	HAL_GPIO_WritePin(BLW_LEDB_GPIO_Port,BLW_LEDB_Pin,SET);  //Green
		        }
		        else if((i>=3)&&(i<=8)){
		         HAL_GPIO_WritePin( LEDConPort[i-1],LEDConPin[i-1],LoadStatus[i]);
		        }

            HAL_Delay(300);
     }

}



void LoadStatusDefault(){


    if(LoadStatus[PW]==0xFF){
    	LoadStatus[PW]=0x00;
    	AT24C32D_write(0x00,&LoadStatus[PW],0);
	 }



	     for(unsigned char i=0;i<=8;i++){

			 if(LoadStatus[i+1]==0xFF){
				LoadStatus[i+1]=0;
				HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i+1]);
				//HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],RESET);
			 }
			 else if(LoadStatus[i+1]==0x01){
				 HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i+1]);
				 //HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],SET);
			 }
			 else if(LoadStatus[i+1]==0x00){
				 HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],LoadStatus[i+1]);
				 //HAL_GPIO_WritePin( RelayConPort[i],RelayConPin[i],RESET);
			 }
	     }


}



/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
