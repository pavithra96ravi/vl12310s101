/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LHT_SW_Pin GPIO_PIN_13
#define LHT_SW_GPIO_Port GPIOC
#define UV_SW_Pin GPIO_PIN_14
#define UV_SW_GPIO_Port GPIOC
#define WA_SW_Pin GPIO_PIN_15
#define WA_SW_GPIO_Port GPIOC
#define MIC_SW_Pin GPIO_PIN_0
#define MIC_SW_GPIO_Port GPIOH
#define SYS_SW_Pin GPIO_PIN_1
#define SYS_SW_GPIO_Port GPIOH

#define PW_LEDB_Pin GPIO_PIN_1
#define PW_LEDA_GPIO_Port GPIOA
#define PW_LEDA_Pin GPIO_PIN_2
#define PW_LEDB_GPIO_Port GPIOA

#define BLW_LEDB_Pin GPIO_PIN_3
#define BLW_LEDA_GPIO_Port GPIOA
#define BLW_LEDA_Pin GPIO_PIN_4
#define BLW_LEDB_GPIO_Port GPIOA


#define LHT_LED_Pin GPIO_PIN_5
#define LHT_LED_GPIO_Port GPIOA
#define UV_LED_Pin GPIO_PIN_6
#define UV_LED_GPIO_Port GPIOA
#define WA_LED_Pin GPIO_PIN_7
#define WA_LED_GPIO_Port GPIOA
#define BLW_CON_Pin GPIO_PIN_0
#define BLW_CON_GPIO_Port GPIOB
#define BLW_SPD_Pin GPIO_PIN_1
#define BLW_SPD_GPIO_Port GPIOB
#define SYS_Pin GPIO_PIN_12
#define SYS_GPIO_Port GPIOB
#define CAM_Pin GPIO_PIN_13
#define CAM_GPIO_Port GPIOB
#define PW_SW_Pin GPIO_PIN_14
#define PW_SW_GPIO_Port GPIOB
#define BLW_SW_Pin GPIO_PIN_15
#define BLW_SW_GPIO_Port GPIOB
#define MIC_LED_Pin GPIO_PIN_8
#define MIC_LED_GPIO_Port GPIOA
#define SYS_LED_Pin GPIO_PIN_11
#define SYS_LED_GPIO_Port GPIOA
#define CAM_LED_Pin GPIO_PIN_12
#define CAM_LED_GPIO_Port GPIOA
#define CAM_SW_Pin GPIO_PIN_15
#define CAM_SW_GPIO_Port GPIOA
#define LHT_Pin GPIO_PIN_4
#define LHT_GPIO_Port GPIOB
#define UV_Pin GPIO_PIN_5
#define UV_GPIO_Port GPIOB
#define WA_Pin GPIO_PIN_6
#define WA_GPIO_Port GPIOB
#define MIC_Pin GPIO_PIN_7
#define MIC_GPIO_Port GPIOB
#define AT24C32D_SCL_Pin GPIO_PIN_8
#define AT24C32D_SCL_GPIO_Port GPIOB
#define AT24C32D_SDA_Pin GPIO_PIN_9
#define AT24C32D_SDA_GPIO_Port GPIOB

#define PW_SW  HAL_GPIO_ReadPin(PW_SW_GPIO_Port,PW_SW_Pin)
#define BLW_SW HAL_GPIO_ReadPin(BLW_SW_GPIO_Port,BLW_SW_Pin)
#define LHT_SW  HAL_GPIO_ReadPin(LHT_SW_GPIO_Port,LHT_SW_Pin)
#define UV_SW  HAL_GPIO_ReadPin(UV_SW_GPIO_Port,UV_SW_Pin)
#define WA_SW  HAL_GPIO_ReadPin(WA_SW_GPIO_Port,WA_SW_Pin)
#define MIC_SW  HAL_GPIO_ReadPin(MIC_SW_GPIO_Port,MIC_SW_Pin)
#define SYS_SW  HAL_GPIO_ReadPin(SYS_SW_GPIO_Port,SYS_SW_Pin)
#define CAM_SW  HAL_GPIO_ReadPin(CAM_SW_GPIO_Port,CAM_SW_Pin)

#define AT24C32D_Add 0x50

#define PW       0
#define BLW_CON  1
#define BLW_SPD  2
#define LHT      3
#define UV  	 4
#define WA       5
#define MIC      6
#define SYS      7
#define CAM      8


#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
